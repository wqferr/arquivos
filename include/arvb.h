#pragma once

#ifndef _ARVORE_B_H_
#define _ARVORE_B_H_

#include <stdint.h>

typedef struct ArvoreB ArvoreB;

ArvoreB *arvb_criar(const char *);
ArvoreB *arvb_abrir(const char *);
void arvb_fechar(ArvoreB *);

int64_t arvb_buscar(ArvoreB *, uint16_t);
void arvb_inserir(ArvoreB *, uint16_t, uint64_t);
void arvb_print(ArvoreB *);

#endif
