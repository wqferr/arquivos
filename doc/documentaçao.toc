\changetocdepth {4}
\select@language {brazil}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {section}{\numberline {1}Descri\IeC {\c c}\IeC {\~a}o das p\IeC {\'a}ginas da \IeC {\'A}rvore B*}{3}{section.0.1}
\contentsline {section}{\numberline {2}Visualiza\IeC {\c c}\IeC {\~a}o grafica da \IeC {\'A}rvore B*}{3}{section.0.2}
\contentsline {section}{\numberline {3}Compila\IeC {\c c}\IeC {\~a}o e utiliza\IeC {\c c}\IeC {\~a}o do programa}{4}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Instru\IeC {\c c}\IeC {\~o}es para compila\IeC {\c c}\IeC {\~a}o utilizando o Makefile}{4}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}Instru\IeC {\c c}\IeC {\~o}es para execu\IeC {\c c}\IeC {\~a}o}{4}{subsection.0.3.2}
\contentsline {section}{\numberline {4}C\IeC {\'o}pia de tela da interface}{4}{section.0.4}
\contentsline {section}{\numberline {5}Bateria de testes}{4}{section.0.5}
