#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "arvb.h"

// Offset de páginas não existentes
#define NIL 0L

// Verifica se uma página dada é ou não folha
#define pagina_folha(p) (p.filhas[0] == NIL)

// Ordem da árvore B
#define ORDEM 4

// Ocupação mínima de uma página
#define OCUPACAO_MIN ((2*ORDEM - 1) / 3)
#define OCUPACAO_MAX (ORDEM - 1)

// Tamanho, em bytes, de uma página de disco
#define TAMANHO_PAGINA 64

// Retornos da função pagina_buscar
#define INEXISTENTE 0
#define PAGINA_ATUAL 1
#define PROX_PAGINA 2

// Um par chave-offset do índice
// __attribute__((__packed__)) impede que o gcc adicione alinhamentos para otimização
typedef struct __attribute__((__packed__)) Par {
    // Chave armazenada
    uint16_t chave;
    // Offset do arquivo de dados
    uint64_t offset;
} Par;

// Uma página da árvore
typedef struct __attribute__((__packed__)) Pagina {
    // Offsets das páginas filhas
    uint64_t filhas[ORDEM];

    // Valores da página
    Par valores[ORDEM - 1];

    // Número de chaves contido na página
    uint8_t nroChaves;

    // Ajuste para fechar 64 bytes
    uint8_t ajuste[1];
} Pagina;


struct ArvoreB {
    // Arquivo aberto para a manipulação da árvore
    FILE *arq;

    // Offset da raiz
    uint64_t raiz;
};

// Reseta o valor de um par a um estado inválido
void par_apagar(Par *p) {
    p->chave = 0;
    p->offset = EOF;
}

// Inicializa uma página com valores iniciais padrão
void pagina_init(Pagina *p) {
    int i;

    p->nroChaves = 0;

    // Evitar avisos de bytes não inicializados
    (void) memset(p->ajuste, 0, sizeof(p->ajuste));

    for (i = 0; i < OCUPACAO_MAX; i++) {
        par_apagar(p->valores + i);
        p->filhas[i] = NIL;
    }
    p->filhas[OCUPACAO_MAX] = NIL;
}

// Busca por uma chave na página dada.
// Se a chave estiver na página, retorna PAGINA_ATUAL, e *pos contém
// a posição no vetor de valores em que está.
// Se a chave estiver em uma subárvore, retorna PROX_PAGINA, e *pos
// contém o índice da filha que é a raiz de tal subárvore.
// Se a chave não estiver na árvore, retorna INEXISTENTE, e *pos não contém
// valor significativo.
int pagina_buscar(Pagina *p, uint16_t chave, int *pos) {
    int i;

    for (i = 0; i < p->nroChaves; i++) {
        if (p->valores[i].chave == chave) {
            *pos = i;
            return PAGINA_ATUAL;
        }

        if (p->valores[i].chave > chave) {
            if (p->filhas[i] == NIL)
                return INEXISTENTE;
            *pos = i;
            return PROX_PAGINA;
        }
    }

    if (p->filhas[i] == NIL)
        return INEXISTENTE;

    *pos = i;
    return PROX_PAGINA;
}

// Retorna se uma página está cheia ou não.
bool pagina_cheia(Pagina *p) {
    return p->nroChaves == OCUPACAO_MAX;
}

// Retorna o primeiro índice do vetor de valores que ainda não
// foi preenchido, ou -1 se a página estiver cheia.
int pagina_menorIndiceLivre(Pagina *p) {
    return pagina_cheia(p) ? -1 : p->nroChaves;
}

// Insere um valor na página p de forma ordenada.
void pagina_inserir(Pagina *p, Par valor) {
    int i = pagina_menorIndiceLivre(p);

    while (i > 0 && p->valores[i-1].chave > valor.chave) {
        p->valores[i] = p->valores[i-1];
        i--;
    }
    p->valores[i] = valor;
    p->nroChaves++;
}

// Salva a página em disco no offset dado.
void pagina_gravar(FILE *fp, Pagina *p, uint64_t offset) {
    (void) fseek(fp, offset, SEEK_SET);
    (void) fwrite(p, 1, sizeof(Pagina), fp);
}

// Recupera uma página da árvore no offset dado.
void pagina_ler(FILE *fp, Pagina *p, uint64_t offset) {
    (void) fseek(fp, offset, SEEK_SET);
    (void) fread(p, 1, sizeof(Pagina), fp);
}

// Tenta fazer a redistribuição de valores da página pai->filhas[indiceFilha] para uma
// de suas irmãs.
// Se foi possível fazer a redistribuição, retorna true. Caso contrário, retorna false.
bool pagina_redistribuir(FILE *fp, Pagina *pai, int indiceFilha, Par valor, uint64_t filhaPromovida) {
    Pagina filha, irma;
    int i, j;

    pagina_ler(fp, &filha, pai->filhas[indiceFilha]);
    if (indiceFilha > 0) {
        pagina_ler(fp, &irma, pai->filhas[indiceFilha-1]);

        // Pode fazer redistribuição para a esquerda
        if (!pagina_cheia(&irma)) {
            i = irma.nroChaves;
            // Rotação do pai para a irmã
            irma.valores[i] = pai->valores[indiceFilha-1];
            irma.filhas[i+1] = filha.filhas[0];
            pai->valores[indiceFilha-1] = filha.valores[0];

            // Ajuste do vetor da filha
            for (j = 0; j < OCUPACAO_MAX-1; j++) {
                filha.valores[j] = filha.valores[j+1];
                filha.filhas[j] = filha.filhas[j+1];
            }
            filha.filhas[ORDEM-2] = filha.filhas[ORDEM-1];
            filha.filhas[ORDEM-1] = NIL;
            par_apagar(filha.valores + OCUPACAO_MAX-1);

            // Inserção ordenada
            j = filha.nroChaves-1;

            while (j > 0 && filha.valores[j-1].chave > valor.chave) {
                filha.valores[j] = filha.valores[j-1];
                filha.filhas[j] = filha.filhas[j-1];

                j--;
            }
            filha.valores[j] = valor;
            filha.filhas[j+1] = filhaPromovida;
            irma.nroChaves++;

            pagina_gravar(fp, &filha, pai->filhas[indiceFilha]);
            pagina_gravar(fp, &irma, pai->filhas[indiceFilha-1]);

            return true;
        }
    }

    // Algoritmo espelhado para a direita
    if (indiceFilha < pai->nroChaves) {
        pagina_ler(fp, &irma, pai->filhas[indiceFilha+1]);

        if (!pagina_cheia(&irma)) {
            i = irma.nroChaves;
            irma.filhas[ORDEM-1] = irma.filhas[ORDEM-2];
            for (j = OCUPACAO_MAX-1; j > 0; j--) {
                irma.filhas[j] = irma.filhas[j-1];
                irma.valores[j] = irma.valores[j-1];
            }
            irma.valores[0] = pai->valores[indiceFilha];
            irma.filhas[0] = filha.filhas[ORDEM-1];
            pai->valores[indiceFilha] = filha.valores[OCUPACAO_MAX-1];
            par_apagar(filha.valores + OCUPACAO_MAX-1);

            j = filha.nroChaves-1;
            while (j > 0 && filha.valores[j-1].chave > valor.chave) {
                filha.valores[j] = filha.valores[j-1];
                filha.filhas[j] = filha.filhas[j-1];

                j--;
            }
            filha.valores[j] = valor;
            filha.filhas[j+1] = filhaPromovida;
            irma.nroChaves++;

            pagina_gravar(fp, &filha, pai->filhas[indiceFilha]);
            pagina_gravar(fp, &irma, pai->filhas[indiceFilha+1]);

            return true;
        }
    }

    // Nenhum dos ramos retornou -> não foi possível redistribuir
    return false;
}

// Realiza o caso especial de split da raiz.
// Retorna o offset em que a raiz nova foi gravada.
// Tanto splitRaiz quanto split foram baseados no código do livro
// File Structures de M. Folk e B. Zoellick, 2a edição.
uint64_t splitRaiz(FILE *fp, Pagina *raiz, uint64_t offsetRaiz, Par valor, uint64_t filhaPromovida) {
    int i;
    Par valores[OCUPACAO_MAX + 1];
    uint64_t filhas[OCUPACAO_MAX + 2];
    uint64_t offsetNovo, offsetNovaRaiz;
    Pagina novo, novaRaiz;

    // Nó que será irmão da raiz antiga
    pagina_init(&novo);
    // Nó que será a nova raiz
    pagina_init(&novaRaiz);

    (void) fseek(fp, 0L, SEEK_END);
    offsetNovo = ftell(fp);

    // Cópia dos valores para o vetor temporário
    (void) memcpy(valores, raiz->valores, OCUPACAO_MAX * sizeof(Par));
    (void) memcpy(filhas, raiz->filhas, ORDEM * sizeof(uint64_t));

    i = OCUPACAO_MAX;
    while (i > 0 && valores[i-1].chave > valor.chave) {
        valores[i] = valores[i-1];
        filhas[i+1] = filhas[i];
        i--;
    }

    valores[i] = valor;
    filhas[i+1] = filhaPromovida;

    // Copia os valores apropriados para cada página
    // Primeira metade -> nó original
    (void) memcpy(raiz->valores, valores, (OCUPACAO_MAX/2) * sizeof(Par));
    (void) memcpy(raiz->filhas, filhas, ((OCUPACAO_MAX/2) + 1) * sizeof(uint64_t));

    // Meio -> raiz nova
    novaRaiz.valores[0] = valores[OCUPACAO_MAX/2];
    novaRaiz.filhas[0] = offsetRaiz;
    novaRaiz.filhas[1] = offsetNovo;

    // Resto -> nó novo
    (void) memcpy(novo.valores, valores + (OCUPACAO_MAX/2 + 1), (OCUPACAO_MAX+1)/2 * sizeof(Par));
    (void) memcpy(novo.filhas, filhas + (OCUPACAO_MAX/2) + 1, (OCUPACAO_MAX/2 + 2) * sizeof(uint64_t));

    for (i = OCUPACAO_MAX/2; i < OCUPACAO_MAX; i++) {
        par_apagar(raiz->valores + i);
        raiz->filhas[i+1] = NIL;
    }

    raiz->nroChaves = OCUPACAO_MAX/2;
    novo.nroChaves = OCUPACAO_MAX/2 + 1;
    novaRaiz.nroChaves = 1;

    pagina_gravar(fp, raiz, offsetRaiz);
    pagina_gravar(fp, &novo, offsetNovo);

    // Somente registrar offset da raiz nova agora pois,
    // anteriormente, SEEK_END retornaria o mesmo offset
    // do nó irmão à raiz antiga. Agora que este já foi escrito,
    // o fim do arquivo foi atualizado com o offset correto.
    (void) fseek(fp, 0L, SEEK_END);
    offsetNovaRaiz = ftell(fp);
    pagina_gravar(fp, &novaRaiz, offsetNovaRaiz);

    return offsetNovaRaiz;
}

// Realiza o split 2-para-3 na página pai->filhas[indiceFilha].
// Prioriza o split com a irmã à direita. Se não houver irmã à direita,
// tenta realizar com a irmã à esquerda.
// Retorna o offset da página nova, e o valor promovido é guardado em *vpromo.
uint64_t split(
                FILE *fp,
                Pagina *pai, int indiceFilha,
                Par valor, uint64_t filhaPromovida,
                Par *vpromo) {
    int i;
    Par valores[2*ORDEM];
    uint64_t filhas[2*ORDEM+1];
    uint64_t offset;
    Pagina filha, irma, nova;

    pagina_init(&nova);

    // filha é a página a ser dividida.
    // É necessário passar o pai para que seja possível recuperar
    // o offset de suas irmãs.
    pagina_ler(fp, &filha, pai->filhas[indiceFilha]);
    (void) fseek(fp, 0L, SEEK_END);
    offset = ftell(fp);

    // filha tem irmã à direita -> tentar split à direita
    if (indiceFilha < pai->nroChaves) {
        pagina_ler(fp, &irma, pai->filhas[indiceFilha+1]);

        // Cópia para vetor temporário
        (void) memcpy(valores, filha.valores, OCUPACAO_MAX * sizeof(Par));
        valores[OCUPACAO_MAX] = pai->valores[indiceFilha];
        (void) memcpy(valores + OCUPACAO_MAX+1, irma.valores, OCUPACAO_MAX * sizeof(Par));
        (void) memcpy(filhas, filha.filhas, ORDEM * sizeof(uint64_t));
        (void) memcpy(filhas + ORDEM, irma.filhas, ORDEM * sizeof(uint64_t));

        // Inserção do valor novo
        i = 2*OCUPACAO_MAX+1;
        while (i > 0 && valor.chave < valores[i-1].chave) {
            valores[i] = valores[i-1];
            filhas[i+1] = filhas[i];
            i--;
        }

        valores[i] = valor;
        filhas[i+1] = filhaPromovida;

        // Cópia do vetor para as páginas apropriadas
        // Primeiro terço -> filha à esquerda (filha)
        (void) memcpy(filha.valores, valores, OCUPACAO_MIN * sizeof(Par));
        (void) memcpy(filha.filhas, filhas, (OCUPACAO_MIN+1) * sizeof(uint64_t));

        // Primeiro elemento promovido sempre acaba entre os 2 nós
        pai->valores[indiceFilha] = valores[OCUPACAO_MIN];

        // Segundo terço -> filha à direita (irma)
        (void) memcpy(irma.valores, valores + OCUPACAO_MIN + 1, OCUPACAO_MIN * sizeof(Par));
        (void) memcpy(irma.filhas, filhas + OCUPACAO_MIN + 1, (OCUPACAO_MIN+1) * sizeof(uint64_t));

        // Segundo valor promovido (pode dar overflow)
        *vpromo = valores[2*OCUPACAO_MIN+1];

        // Terceiro terço -> página nova
        (void) memcpy(nova.valores, valores + 2*OCUPACAO_MIN + 2, OCUPACAO_MIN * sizeof(Par));
        (void) memcpy(nova.filhas, filhas + 2*OCUPACAO_MIN + 2, (OCUPACAO_MIN+1) * sizeof(uint64_t));

        // Zerar posições não utilizadas
        for (i = OCUPACAO_MIN + 1; i < OCUPACAO_MAX; i++) {
            par_apagar(filha.valores + i);
            par_apagar(irma.valores + i);
        }

        // Zerar ponteiros antigos
        for (i = OCUPACAO_MIN + 2; i < ORDEM; i++) {
            filha.filhas[i] = NIL;
            irma.filhas[i] = NIL;
        }

        filha.nroChaves = OCUPACAO_MIN;
        irma.nroChaves = OCUPACAO_MIN;
        nova.nroChaves = OCUPACAO_MIN;

        // filha e nova são gravadas posteriormente.
        // pai é gravada no retorno da função para que não seja
        // necessário passar seu offset também.
        pagina_gravar(fp, &irma, pai->filhas[indiceFilha+1]);
    } else {
        // Se for a última página à direita, então tem irmã à esquerda.
        // Algoritmo acima espelhado.
        pagina_ler(fp, &irma, pai->filhas[indiceFilha-1]);

        // Cópia para o vetor temporário
        (void) memcpy(valores, irma.valores, OCUPACAO_MAX * sizeof(Par));
        valores[OCUPACAO_MAX] = pai->valores[indiceFilha-1];
        (void) memcpy(valores + OCUPACAO_MAX + 1, filha.valores, OCUPACAO_MAX * sizeof(Par));
        (void) memcpy(filhas, irma.filhas, ORDEM * sizeof(uint64_t));
        (void) memcpy(filhas + ORDEM, filha.filhas, ORDEM * sizeof(uint64_t));

        // Inserção ordenada do valor novo
        i = 2*OCUPACAO_MAX+1;
        while (i > 0 && valor.chave < valores[i-1].chave) {
            valores[i] = valores[i-1];
            filhas[i+1] = filhas[i];
            i--;
        }

        valores[i] = valor;
        filhas[i+1] = filhaPromovida;

        // Primeiro terço -> irmã (agora à esquerda de filha)
        (void) memcpy(irma.valores, valores, OCUPACAO_MIN * sizeof(Par));
        (void) memcpy(irma.filhas, filhas, (OCUPACAO_MIN+1) * sizeof(uint64_t));

        // Primeiro valor promovido vai para o espaço entre as filhas
        pai->valores[indiceFilha-1] = valores[OCUPACAO_MIN];

        // Segundo terço -> filha (agora à direita)
        (void) memcpy(filha.valores, valores + OCUPACAO_MIN + 1, OCUPACAO_MIN * sizeof(Par));
        (void) memcpy(filha.filhas, filhas + OCUPACAO_MIN + 1, (OCUPACAO_MIN+1) * sizeof(uint64_t));

        // Segundo valor promovido pode dar overflow
        *vpromo = valores[2*OCUPACAO_MIN + 1];

        // Terceiro terço -> página nova
        (void) memcpy(nova.valores, valores + 2*OCUPACAO_MIN + 2, OCUPACAO_MIN * sizeof(Par));
        (void) memcpy(nova.filhas, filhas + 2*OCUPACAO_MIN + 2, (OCUPACAO_MIN+1) * sizeof(uint64_t));

        for (i = OCUPACAO_MIN + 1; i < OCUPACAO_MAX; i++) {
            par_apagar(filha.valores + i);
            par_apagar(irma.valores + i);
        }

        for (i = OCUPACAO_MIN + 2; i < ORDEM; i++) {
            filha.filhas[i] = NIL;
            irma.filhas[i] = NIL;
        }

        filha.nroChaves = OCUPACAO_MIN;
        irma.nroChaves = OCUPACAO_MIN;
        nova.nroChaves = OCUPACAO_MIN;

        pagina_gravar(fp, &irma, pai->filhas[indiceFilha-1]);
    }

    pagina_gravar(fp, &nova, offset);
    pagina_gravar(fp, &filha, pai->filhas[indiceFilha]);

    return offset;
}


// Inicializa os campos de uma árvore recém criada.
void _arvb_init(ArvoreB *a) {
    Pagina raiz;

    pagina_init(&raiz);

    // Primeira posição alinhada às páginas de disco após o offset 0,
    // utilizado para o registro de cabeçalho.
    a->raiz = sizeof(Pagina);

    (void) fwrite(&a->raiz, sizeof(a->raiz), 1, a->arq);
    pagina_gravar(a->arq, &raiz, a->raiz);
}

// Atualiza a posição da raiz no registro de cabeçalho.
void _arvb_update(ArvoreB *a) {
    (void) fseek(a->arq, 0L, SEEK_SET);
    (void) fwrite(&a->raiz, sizeof(a->raiz), 1, a->arq);
}

// Cria ou sobrescreve uma árvore no arquivo dado.
ArvoreB *arvb_criar(const char *arq) {
    ArvoreB *a = (ArvoreB *) malloc(sizeof(*a));

    a->arq = fopen(arq, "wb+"); // Abrir em modo w -> apagar arquivo se já existir
    _arvb_init(a);

    return a;
}

// Abre uma árvore existente ou cria uma nova no arquivo dado.
ArvoreB *arvb_abrir(const char *arq) {
    FILE *f = fopen(arq, "rb+");
    ArvoreB *a;

    if (f == NULL) { // Arquivo não existe
        a = arvb_criar(arq); // Criar árvore nova
    } else { // Arquivo já existe
        a = malloc(sizeof(*a));
        a->arq = f;
        (void) fread(&a->raiz, sizeof(a->raiz), 1, f); // Recuperar registro de cabeçalho
    }

    return a;
}

// Libera os recursos usados pela árvore.
void arvb_fechar(ArvoreB *a) {
    fclose(a->arq);
    free(a);
}



// Retorna o offset armazenado juntamente à chave dada.
// Se a chave não foi registrada no índice, retorna EOF.
int64_t arvb_buscar(ArvoreB *a, uint16_t chave) {
    Pagina p;
    int i, caso;
    uint64_t offset = 0;
    bool achou = false;

    pagina_ler(a->arq, &p, a->raiz);

    do {
        caso = pagina_buscar(&p, chave, &i);

        switch (caso) {
            case INEXISTENTE:
                offset = EOF;
                achou = true;
                break;

            case PAGINA_ATUAL:
                offset = p.valores[i].offset;
                achou = true;
                break;

            case PROX_PAGINA:
                pagina_ler(a->arq, &p, p.filhas[i]);
                break;
        }
    } while (!achou);

    return offset;
}

// Função recursiva para inserção de um elemento na árvore.
// Baseado no algoritmo em File Structures, M. Folk e B. Zoellick, 2a edição.
bool _arvb_inserir(
                FILE *fp, uint64_t offsetPg,
                Par valor, Par *vpromo,
                uint64_t *filhaPromovida) {
    Pagina p;
    int i = 0;
    int caso;
    Par vpb;
    uint64_t filhaPromovidaBaixo;

    pagina_ler(fp, &p, offsetPg);

    if (pagina_folha(p)) {
        if (pagina_cheia(&p)) {
            // Folha cheia -> voltar ao pai.
            return false;
        } else {
            // Folha não cheia -> inserir
            pagina_inserir(&p, valor);
            pagina_gravar(fp, &p, offsetPg);
            return true;
        }
    } else {
        caso = pagina_buscar(&p, valor.chave, &i);
        if (caso == PAGINA_ATUAL) { // encontrou o valor a ser inserido nesta página
            // Inserção duplicada -> atualizar valor associado
            p.valores[i] = valor;
            pagina_gravar(fp, &p, offsetPg);
            return true;
        } else if (_arvb_inserir(fp, p.filhas[i], valor, &vpb, &filhaPromovidaBaixo)) {
            // Foi possível inserir na subárvore especificada.
            return true;
        } else if (pagina_redistribuir(fp, &p, i, valor, filhaPromovidaBaixo)) {
            // Foi possível fazer a redistribuição.
            pagina_gravar(fp, &p, offsetPg);
            return true;
        } else {
            // Não foi possível evitar o split
            *filhaPromovida = split(fp, &p, i, valor, filhaPromovidaBaixo, &vpb);

            // Pai cheio -> não é possível inserir o valor promovido.
            // Promover de volta ao pai.
            if (pagina_cheia(&p)) {
                *vpromo = vpb;
                pagina_gravar(fp, &p, offsetPg);
                return false;
            } else {
                // Pai não cheio -> inserir onde apropriado.
                i = p.nroChaves;
                while (i > 0 && p.valores[i-1].chave > vpb.chave) {
                    p.valores[i] = p.valores[i-1];
                    p.filhas[i+1] = p.filhas[i];
                    i--;
                }
                p.valores[i] = vpb;
                p.filhas[i+1] = *filhaPromovida;
                p.nroChaves++;
                pagina_gravar(fp, &p, offsetPg);

                return true;
            }
        }
    }
}

// Função que inicia a corrente de chamadas recursivas de _arvb_inserir.
// Quase o mesmo algoritmo, mas pequenas alterações permitem que cuide de
// overflows sem delegar ao pai (já que, obviamente, seria inviável).
void arvb_inserir(ArvoreB *a, uint16_t chave, uint64_t offset) {
    int caso, i = 0;
    uint64_t filhaPromovidaBaixo = NIL;
    Pagina raiz;
    Par valor, vpb;

    valor.chave = chave;
    valor.offset = offset;

    pagina_ler(a->arq, &raiz, a->raiz);

    if (pagina_folha(raiz)) {
        if (pagina_cheia(&raiz)) {
            // Raiz folha cheia -> criar nova raiz.
            a->raiz = splitRaiz(a->arq, &raiz, a->raiz, valor, NIL);
        } else {
            pagina_inserir(&raiz, valor);
            pagina_gravar(a->arq, &raiz, a->raiz);
        }
    } else {
        caso = pagina_buscar(&raiz, valor.chave, &i);
        if (caso == PAGINA_ATUAL) {
            // Inserção duplicada.
            raiz.valores[i] = valor;
            pagina_gravar(a->arq, &raiz, a->raiz);
        } else if (!_arvb_inserir(a->arq, raiz.filhas[i], valor, &vpb, &filhaPromovidaBaixo)) {
            if (pagina_redistribuir(a->arq, &raiz, i, valor, filhaPromovidaBaixo)) {
                // Foi possivel redistribuir.
                pagina_gravar(a->arq, &raiz, a->raiz);
            } else {
                // Split inevitável
                filhaPromovidaBaixo = split(a->arq, &raiz, i, valor, filhaPromovidaBaixo, &vpb);

                if (pagina_cheia(&raiz)) {
                    // Não é possível inserir o valor promovido, e não é possível
                    // delegar ao pai -> criar raiz nova e inserir o valor promovido.
                    a->raiz = splitRaiz(a->arq, &raiz, a->raiz, vpb, filhaPromovidaBaixo);
                } else {
                    // Insere o valor promovido na raiz.
                    i = raiz.nroChaves;
                    while (i > 0 && raiz.valores[i-1].chave > vpb.chave) {
                        raiz.valores[i] = raiz.valores[i-1];
                        raiz.filhas[i+1] = raiz.filhas[i];
                        i--;
                    }

                    raiz.valores[i] = vpb;
                    raiz.filhas[i+1] = filhaPromovidaBaixo;
                    raiz.nroChaves++;

                    pagina_gravar(a->arq, &raiz, a->raiz);
                }
            }
        }
    }

    _arvb_update(a);
}

// Função recursiva para imprimir a árvore.
// A saída é da forma de chaves aninhadas. Cada par [chave, offset] é
// dado entre colchetes.
void _arvb_print(FILE *fp, uint64_t offset) {
    int i;
    Pagina p;

    if (offset != NIL) {
        pagina_ler(fp, &p, offset);
        printf("{");
        for (i = 0; i < p.nroChaves; i++) {
            if (p.filhas[i] != NIL)
                _arvb_print(fp, p.filhas[i]);
            printf("[%hd %lu]", p.valores[i].chave, p.valores[i].offset);
        }
        _arvb_print(fp, p.filhas[i]);
        printf("}");
    }
}

// Exibe a árvore completa usando a função acima.
void arvb_print(ArvoreB *a) {
    _arvb_print(a->arq, a->raiz);
}
